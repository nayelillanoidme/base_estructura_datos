use tienda_virtual
CREATE PROCEDURE InsertarProducto
    @NombreProducto NVARCHAR(50),
    @Precio DECIMAL(10, 2)
AS
BEGIN
    INSERT INTO Productos (Nombre, Precio)
    VALUES (@NombreProducto, @Precio);
END;
------Consultar Datos
CREATE PROCEDURE ConsultarProductoPorID
    @ProductoID INT
AS
BEGIN
    SELECT Nombre, Precio
    FROM Productos
    WHERE ProductoID = @ProductoID;
END;
--------Modificar Datos

CREATE PROCEDURE ActualizarPrecioProducto
    @ProductoID INT,
    @NuevoPrecio DECIMAL(10, 2)
AS
BEGIN
    UPDATE Productos
    SET Precio = @NuevoPrecio
    WHERE ProductoID = @ProductoID;
END;
--- Eliminar Datos:

CREATE PROCEDURE EliminarProducto
    @ProductoID INT
AS
BEGIN
    DELETE FROM Productos
    WHERE ProductoID = @ProductoID;
END;