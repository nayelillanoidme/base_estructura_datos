use clinica
CREATE TABLE Empleados (
    Emp_No INT PRIMARY KEY,
    Apellido NVARCHAR(50),
    Oficio NVARCHAR(50),
    Dir INT,
    Fecha_Alt DATE,
    Comision DECIMAL(10, 2),
    Dept_No INT
);

INSERT INTO Empleados (Emp_No, Apellido, Oficio, Dir, Fecha_Alt, Comision, Dept_No)
VALUES
    (7369, 'nayeli', 'llano', 7902, '1980-12-17', 800, 20),
    (7499, 'dani', 'idme', 7698, '1981-02-20', 1600, 30),
    (7521, 'framk', 'yanqui', 7698, '1981-02-22', 1250, 30);

CREATE TABLE Departamento (
    Dept_No INT PRIMARY KEY,
    DNombre NVARCHAR(50),
    Loc NVARCHAR(50)
);


INSERT INTO Departamento (Dept_No, DNombre, Loc)
VALUES
    (10, 'Ventas', 'Puno'),
    (20, 'Recursos Humanos', 'Juliaca'),
    (30, 'Marketing', 'Pucara');


	CREATE TABLE Enfermo (
    Inscripcion INT PRIMARY KEY,
    Apellido NVARCHAR(50),
    Direccion NVARCHAR(100),
    Fecha_Nac DATE,
    Sexo CHAR(1),
    NSS NVARCHAR(20)
);
go

INSERT INTO Enfermo (Inscripcion, Apellido, Direccion, Fecha_Nac, Sexo, NSS)
VALUES
    (1, 'Sanchez', '123 Amazonas', '1990-05-15', 'M', '123-45-6789'),
    (2, 'Arapa', '456 Alejandro Peralta', '1985-09-20', 'F', '987-65-4321'),
	(3, 'Mamani','123 Deustua','2003-10-04','M','525-12-6352');

	

ALTER TABLE Empleados
ADD CONSTRAINT UQ_Emp_No UNIQUE (Emp_No);
	
ALTER TABLE Departamento
ADD CONSTRAINT UQ_Dept_No UNIQUE (Dept_No);

-- Crear un procedimiento almacenado
CREATE PROCEDURE ObtenerEmpleados(
    @Dept_No INT
)
AS
BEGIN
    SELECT *
    FROM Empleados
    WHERE YEAR(Fecha_Alt) < 2018 AND Dept_No = @Dept_No;
END;

-----
CREATE PROCEDURE InsertarDepartamento(
    @Dept_No INT,
    @DNombre NVARCHAR(50),
    @Loc NVARCHAR(50)
)
AS
BEGIN
    INSERT INTO Departamento (Dept_No, DNombre, Loc)
    VALUES (@Dept_No, @DNombre, @Loc);
END;

----

CREATE PROCEDURE CalcularPromedioEdadPorDepartamento
AS
BEGIN
    SELECT E.Dept_No, D.DNombre, 
           AVG(DATEDIFF(YEAR, E.Fecha_Alt, GETDATE())) AS PromedioEdad
    FROM Empleados E
    JOIN Departamento D ON E.Dept_No = D.Dept_No
    GROUP BY E.Dept_No, D.DNombre;
END;

--
CREATE PROCEDURE ObtenerInformacionEmpleado(
    @Emp_No INT
)
AS
BEGIN
    SELECT Apellido, Oficio, Comision
    FROM Empleados
    WHERE Emp_No = @Emp_No;
END;

-- 
CREATE PROCEDURE DarDeBajaEmpleado(
    @Apellido NVARCHAR(50)
)
AS
BEGIN
    DELETE FROM Empleados
    WHERE Apellido = @Apellido;
END;

-------part 2

------------
SELECT E.Apellido AS NombreEmpleado, E.Oficio, D.DNombre AS Departamento, E.Especialidad
FROM Empleados E
JOIN Departamento D ON E.Dept_No = D.Dept_No
WHERE E.Dept_No = 30; -- Cambia el n�mero del departamento seg�n tus necesidades

-----
SELECT Apellido AS NombreEmpleado
FROM Empleados
WHERE Apellido IN (SELECT Apellido FROM Enfermo);

-----
CREATE PROCEDURE ObtenerPacientesPorDepartamento(@Dept_No INT)
AS
BEGIN
    SELECT E.Apellido AS NombreEmpleado, E.Oficio, D.DNombre AS Departamento
    FROM Empleados E
    JOIN Departamento D ON E.Dept_No = D.Dept_No
    WHERE E.Dept_No = @Dept_No;
END;

CREATE VIEW VistaEmpleadosYPacientes AS
SELECT E.Apellido AS NombreEmpleado, E.Oficio, D.DNombre AS Departamento, 'Empleado' AS Tipo
FROM Empleados E
JOIN Departamento D ON E.Dept_No = D.Dept_No
UNION ALL
SELECT Apellido AS NombrePaciente, NULL, NULL, 'Paciente'
FROM Enfermo;


