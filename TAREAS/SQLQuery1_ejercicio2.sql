use Ejercicio2
--  tabla HOSPITAL
CREATE TABLE hospital (
    hospital_cod INT NOT NULL,
    nombre VARCHAR(10),
    direccion VARCHAR(20),
    telefono VARCHAR(8),
    num_cama INT
);
--  tabla SALA
CREATE TABLE SALA (
    HOSPITAL_COD INT NOT NULL,
    SALA_COD INT NOT NULL,
    NOMBRE VARCHAR(20),
    NUM_CAMA INT
);

--  tabla PLANTILLA
CREATE TABLE PLANTILLA (
    HOSPITAL_COD INT NOT NULL,
    SALA_COD INT NOT NULL,
    EMPLEADO_NO INT NOT NULL,
    APELLIDO VARCHAR(15),
    FUNCION VARCHAR(10),
    TURNO VARCHAR(1),
    SALARIO INT
);

--  tabla OCUPACION
CREATE TABLE OCUPACION (
    INSCRIPCION INT NOT NULL,
    HOSPITAL_COD INT NOT NULL,
    SALA_COD INT NOT NULL,
    CAMA INT
);

--  tabla DOCTOR
CREATE TABLE DOCTOR (
    HOSPITAL_COD INT NOT NULL,
    DOCTOR_NO INT NOT NULL,
    APELLIDO VARCHAR(13),
    ESPECIALIDAD VARCHAR(16)
);

--  tabla ENFERMO
CREATE TABLE ENFERMO (
    INSCRIPCION INT NOT NULL,
    APELLIDO VARCHAR(15),
    DIRECCION VARCHAR(20),
    FECHA_NAC DATE,
    S VARCHAR(1),
    NSS INT
);

--  tabla EMP
CREATE TABLE EMP (
    EMP_NO INT,
    APELLIDO VARCHAR(10),
    OFICIO VARCHAR(10),
    DIR INT,
    FECHA_ALTA DATE,
    SALARIO INT,
    COMISION INT,
    DEPT_NO INT
);

--  tabla DEPT2
CREATE TABLE DEPT2 (
    DEPT_NO INT,
    DNOMBRE VARCHAR(14),
    LOC VARCHAR(14)
);

/*ELIMINAMOS LA TABLA POR QUE ESTA MAL XD*/

DROP TABLE hospital;

CREATE TABLE HOSPITAL (
    HOSPITAL_COD INT NOT NULL,
    NOMBRE VARCHAR(10),
    DIRECCION VARCHAR(20),
    TELEFONO VARCHAR(8),
    NUM_CAMA INT
);

INSERT INTO HOSPITAL (HOSPITAL_COD, NOMBRE, DIRECCION, TELEFONO, NUM_CAMA)
VALUES
    (13, 'Provincial', 'Donell 50', '964-4264', 502),
    (18, 'General Atocha', 's/n', '595-3111', 987),
    (22, 'La Paz', 'Castellana 1000', '923-5411', 412),
    (45, 'San Carlos', 'Ciudad Universitaria', '597-1500', 845);
	

INSERT INTO SALA (HOSPITAL_COD, SALA_COD, NOMBRE, NUM_CAMA)
VALUES
    (13, 3, 'Cuidados Intensivos', 21),
    (13, 6, 'Psiqui�trico', 67),
    (18, 3, 'Cuidados Intensivos', 10),
    (18, 4, 'Cardiolog�a', 53),
    (22, 1, 'Recuperaci�n', 10),
    (22, 6, 'Psiqui�trico', 118),
    (22, 2, 'Maternidad', 34),
    (45, 4, 'Cardiolog�a', 55),
    (45, 1, 'Recuperaci�n', 13),
    (45, 2, 'Maternidad', 24);


INSERT INTO PLANTILLA (HOSPITAL_COD, SALA_COD, EMPLEADO_NO, APELLIDO, FUNCION, TURNO, SALARIO)
VALUES
    (13, 6, 3754, 'Diaz B.', 'Enfermera', 'T', 2262000),
    (13, 6, 3106, 'Hernandez J.', 'Enfermero', 'T', 2755000),
    (18, 4, 6357, 'Karplus W.', 'Interno', 'T', 3379000),
    (22, 6, 1009, 'Higueras D.', 'Enfermera', 'T', 2005000),
    (22, 6, 8422, 'Bocina G.', 'Enfermero', 'M', 1638000),
    (22, 2, 9901, 'Nu�ez C.', 'Interno', 'M', 2210000),
    (22, 1, 6065, 'Rivera G.', 'Enfermera', 'N', 1626000),
    (22, 1, 7379, 'Carlos R.', 'Enfermera', 'T', 2119000),
    (45, 4, 1280, 'Amigo R.', 'Interno', 'N', 2210000),
    (45, 1, 8526, 'Frank H.', 'Enfermera', 'T', 2522000);
    

INSERT INTO OCUPACION (INSCRIPCION, HOSPITAL_COD, SALA_COD, CAMA)
VALUES
    (10995, 13, 3, 1),
    (18004, 13, 3, 2),
    (14024, 13, 3, 3),
    (36658, 18, 4, 1),
    (38702, 18, 4, 2),
    (39217, 22, 6, 1),
    (59076, 22, 6, 2),
    (63827, 22, 6, 3),
    (64823, 22, 2, 1);

INSERT INTO DOCTOR (HOSPITAL_COD, DOCTOR_NO, APELLIDO, ESPECIALIDAD)
VALUES
    (13, 435, 'Lopez A.', 'Cardiolog�a'),
    (18, 585, 'Miller G.', 'Ginecolog�a'),
    (18, 982, 'Cajal R.', 'Cardiolog�a'),
    (22, 453, 'Galo D.', 'Pediatr�a'),
    (22, 398, 'Best K.', 'Urolog�a'),
    (22, 386, 'Cabeza D.', 'Psiquiatr�a'),
    (45, 607, 'Niqo P.', 'Pediatr�a');



INSERT INTO ENFERMO (INSCRIPCION, APELLIDO, DIRECCION, FECHA_NAC, S, NSS)
VALUES
    (10995, 'Laguia M.', 'Recoletos 50', '1967-06-23', 'M', 280862482),
    (18004, 'Serrano V.', 'Alcala 12', '1960-05-21', 'F', 284991452),
    (14024, 'Fernandez M', 'Recoletos 50', '1967-06-23', 'F', 321790059),
    (36658, 'Domin S.', 'Mayor 71', '1942-01-01', 'M', 160657471),
    (38702, 'Neal R.', 'Orense 11', '1940-06-18', 'F', 380010217),
    (39217, 'Cervantes M.', 'Peron 38', '1952-02-29', 'M', 440294390),
    (59076, 'Miller G.', 'Lopez de Hoyos 2', '1945-09-16', 'F', 311969044),
    (63827, 'Ruiz P.', 'Esquerdo 103', '1980-12-26', 'M', 100973253),
    (64823, 'Fraser A.', 'Soto 3', '1980-07-10', 'F', 285201776),
    (74835, 'Benitez E.', 'Argentina 5', '1957-10-05', 'M', 154811767);


INSERT INTO EMP (EMP_NO, APELLIDO, OFICIO, DIR, FECHA_ALTA, SALARIO, COMISION, DEPT_NO)
VALUES
    (7369, 'SANCHEZ', 'EMPLEADO', 20, '1980-12-17', 104000, NULL, 20),
    (7499, 'ARROYO', 'VENDEDOR', 7698, '1981-02-20', 208000, 39000, 30),
    (7521, 'SALA', 'VENDEDOR', 7698, '1981-02-22', 162500, 65000, 30),
    (7566, 'JIMENEZ', 'DIRECTOR', 7839, '1981-04-02', 386750, NULL, 20),
    (7654, 'ARENAS', 'VENDEDOR', 7698, '1981-09-28', 162500, 182000, 30),
    (7698, 'NEGRO', 'DIRECTOR', 7839, '1981-05-01', 370500, NULL, 30),
    (7782, 'CEREZO', 'DIRECTOR', 7839, '1981-06-09', 318500, NULL, 10),
    (7788, 'GIL', 'ANALISTA', 7566, '1982-12-09', 390000, NULL, 20),
    (7839, 'REY', 'PRESIDENTE', NULL, '1981-11-17', 650000, NULL, 10),
    (7844, 'TOVAR', 'VENDEDOR', 7698, '1981-09-08', 195000, 0, 30),
    (7876, 'ALONSO', 'EMPLEADO', 7788, '1983-01-12', 143000, NULL, 20),
    (7900, 'JIMENO', 'EMPLEADO', 7698, '1981-12-03', 123500, NULL, 30),
    (7902, 'FERNANDEZ', 'ANALISTA', 7566, '1981-12-03', 390000, NULL, 20),
    (7934, 'MU�OZ', 'EMPLEADO', 7782, '1982-01-23', 169000, NULL, 10);

INSERT INTO DEPT2 (DEPT_NO, DNOMBRE, LOC)
VALUES
    (10, 'CONTABILIDAD', 'MADRID'),
    (20, 'INVESTIGACI�N', 'BILBAO'),
    (30, 'VENTAS', 'SEVILLA'),
    (40, 'OPERACIONES', 'MALAGA');

	use Ejercicio2

---clausula where
--1
SELECT APELLIDO
FROM PLANTILLA
WHERE UPPER(APELLIDO) LIKE 'H%';
--2
SELECT APELLIDO
FROM PLANTILLA
WHERE UPPER(FUNCION) IN ('ENFERMERO', 'ENFERMERA')
AND UPPER(TURNO) IN ('T', 'M');

--3
SELECT APELLIDO, SALARIO
FROM PLANTILLA
WHERE SALARIO BETWEEN 2000000 AND 2500000
AND UPPER(FUNCION) = 'ENFERMERA';

---4
SELECT SUBSTRING(NOMBRE, 1, 3) AS ABR, HOSPITAL_COD, NOMBRE
FROM HOSPITAL
ORDER BY ABR;
--5
SELECT

  apellido,

  especialidad,

  CASE

    WHEN especialidad = 'Cardiolog�a' THEN '1'

    WHEN especialidad = 'Psiquiatr�a' THEN '2'

    WHEN especialidad = 'Pediatr�a' THEN '3'

    ELSE '4'

  END AS C�DIGO

FROM doctor;

--6
SELECT APELLIDO, CHARINDEX('A', APELLIDO, 1) AS "PRIMERA LETRA A"
FROM ENFERMO;

---7
SELECT 'El DEPARTAMENTO DE ' + DNOMBRE + ' EN ESTA ' + LOC AS COMENTARIO
FROM DEPT2;

---8
SELECT 

  SUBSTRING(apellido, 1, CHARINDEX('N', apellido) - 1) + 'nnn' + 

  SUBSTRING(apellido, CHARINDEX('N', apellido) + 1, LEN(apellido)) AS [TRES N]

FROM emp

WHERE CHARINDEX('N', apellido) > 0

ORDER BY apellido;
--9
SELECT 
    apellido,
    salario + ISNULL(comision, 0) AS sal_total,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 1, 1) AS c,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 2, 1) AS d,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 3, 1) AS m,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 4, 1) AS c2,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 5, 1) AS d2,
    SUBSTRING(CAST(salario + ISNULL(comision, 0) AS VARCHAR), 6, 1) AS u
FROM emp
ORDER BY 2 DESC, 1;

--10

SELECT
    apellido,
    oficio,
    salario + ISNULL(comision, salario * 0.15) AS salario_total
FROM emp
WHERE comision IS NULL OR comision > salario * 0.15
ORDER BY oficio, salario_total DESC;

------OPERADORES Y FUNCIONES DE FECHAS
--11
SELECT
    apellido,
    ROUND (salario / 12,0) AS "SALARIO MENSUAL"
FROM plantilla
WHERE UPPER(funcion) IN ('ENFERMERA', 'ENFERMERO');

--12
SELECT DATEADD(DAY, -21, GETDATE()) AS fecha;

USE Ejercicio2
--13
SELECT apellido, oficio,
       CONVERT(VARCHAR, fecha_alta, 106) AS alta
FROM EMP
WHERE dept_no = 20 AND salario > 150000;
--14

SELECT APELLIDO, OFICIO, DATENAME(WEEKDAY, FECHA_ALTA) AS DIA
FROM EMP
WHERE DATEPART(WEEKDAY, FECHA_ALTA) IN (3, 4, 5)
ORDER BY OFICIO;

--15

SELECT
    APELLIDO,
    OFICIO,
    CASE
        WHEN DATENAME(WEEKDAY, FECHA_ALTA) = 'Monday' THEN 'Lunes'
        WHEN DATENAME(WEEKDAY, FECHA_ALTA) = 'Tuesday' THEN 'Martes'
        WHEN DATENAME(WEEKDAY, FECHA_ALTA) = 'Wednesday' THEN 'Miercoles'
        WHEN DATENAME(WEEKDAY, FECHA_ALTA) = 'Thursday' THEN 'Jueves'
        WHEN DATENAME(WEEKDAY, FECHA_ALTA) = 'Friday' THEN 'Viernes'
        ELSE 'Fin de semana'
    END AS DIA
FROM EMP
ORDER BY 3;

--16
select avg(salario) "SALARIO MEDIO"
from emp
where upper(oficio) = 'ANALISTA';
--17
SELECT MAX(salario) AS maximo, MIN(salario) AS minimo, MAX(salario) - MIN(salario) AS diferencia
FROM emp;
--18
SELECT COUNT(DISTINCT oficio) AS tareas
FROM emp
WHERE dept_no IN (10, 20);
--19
SELECT dept_no, oficio, COUNT(*) AS cantidad
FROM emp
GROUP BY dept_no, oficio;
--20
SELECT dept_no, COUNT(*) AS personas
FROM emp
GROUP BY dept_no
HAVING COUNT(*) > 4;
--21
SELECT dept_no, oficio, COUNT(*) AS personas
FROM emp
GROUP BY dept_no, oficio
HAVING COUNT(*) > 2;
--22
SELECT dept_no, COUNT(*) AS empleados
FROM emp
WHERE UPPER(oficio) = 'EMPLEADO'
GROUP BY dept_no;
--23

SELECT oficio, AVG(salario + ISNULL(comision, 0)) AS "SALARIO MEDIO ANUAL"
FROM emp
WHERE UPPER(oficio) = 'VENDEDOR'
GROUP BY oficio;
--24
SELECT oficio, AVG(salario + ISNULL(comision, 0)) AS "SALARIO MEDIO ANUAL"
FROM emp
WHERE UPPER(oficio) IN ('VENDEDOR', 'EMPLEADO')
GROUP BY oficio;
--25
SELECT dept_no, oficio, SUM(salario) AS suma, MAX(salario) AS maximo
FROM emp
WHERE (comision IS NULL OR comision < 0.25 * salario)
GROUP BY dept_no, oficio
HAVING SUM(salario) >= 0.5 * (
    SELECT MAX(salario)
    FROM emp e2
    WHERE e2.dept_no = emp.dept_no AND e2.oficio = emp.oficio
);
--26

SELECT YEAR(fecha_alta) AS alta, oficio,
       COUNT(*) AS "N1 EMPL", AVG(salario) AS "MEDIA SALARIAL"
FROM emp
WHERE DATEPART(WEEKDAY, fecha_alta) NOT IN (1, 7)
GROUP BY YEAR(fecha_alta), oficio
HAVING COUNT(*) > 1
ORDER BY alta, "MEDIA SALARIAL" DESC;

--27
SELECT SUBSTRING(apellido, 1, 1) AS inicial, MAX(salario) AS maximo
FROM emp
WHERE CHARINDEX('N', apellido) = 0
      AND UPPER(SUBSTRING(apellido, 1, 1)) NOT IN ('A', 'E', 'I', 'O', 'U')
GROUP BY SUBSTRING(apellido, 1, 1)
ORDER BY inicial;

----28
SELECT
    oficio,
    SUM(CASE WHEN dept_no = 10 THEN salario ELSE 0 END) AS dep10,
    SUM(CASE WHEN dept_no = 20 THEN salario ELSE 0 END) AS dep20,
    SUM(CASE WHEN dept_no = 30 THEN salario ELSE 0 END) AS dep30,
    SUM(CASE WHEN dept_no = 40 THEN salario ELSE 0 END) AS dep40,
    SUM(salario) AS total
FROM emp
GROUP BY oficio
ORDER BY total DESC;
---29
SELECT DEPT_NO, OFICIO,
SUM(ISNULL(COMISION, 
    CASE 
        WHEN DEPT_NO = 10 THEN 0.1 * SALARIO
        WHEN DEPT_NO = 20 THEN 0.15 * SALARIO
        WHEN DEPT_NO = 30 THEN 0.17 * SALARIO
        ELSE 0.05 * SALARIO
    END
)) AS "SUMA DE COMISIONES"
FROM EMP
WHERE YEAR(FECHA_ALTA) <= 1981 AND UPPER(OFICIO) != 'PRESIDENTE'
GROUP BY DEPT_NO, OFICIO;
--30
SELECT 'Maximo---' AS comentario, MAX(SALARIO) AS valor, DEPT_NO
FROM EMP
GROUP BY DEPT_NO
UNION
SELECT 'Media--->' AS comentario, AVG(SALARIO) AS valor, DEPT_NO
FROM EMP
GROUP BY DEPT_NO
UNION
SELECT 'M�nimo---' AS comentario, MIN(SALARIO) AS valor, DEPT_NO
FROM EMP
GROUP BY DEPT_NO
ORDER BY 3;
--31
select apellido, oficio, e.dept_no, dnombre
from emp e, dept2 d
where e.dept_no = d.dept_no;
---32
SELECT APELLIDO, OFICIO, LOC
FROM EMP E, DEPT2 D
WHERE E.DEPT_NO = D.DEPT_NO
AND UPPER(OFICIO) IN ('ANALISTA');
---33
select e.dept_no num_dep, dnombre departamento, count(*) n1_empl
from emp e, dept2 d
where e.dept_no = d.dept_no
group by e.dept_no, dnombre
order by 3 desc;
--34
select dnombre, max(salario) maximo, min(salario) m�nimo,
avg(salario) media, count(*) n_empl
from emp e, dept2 d
where upper(dnombre) in ('VENTAS', 'CONTABILIDAD')
and e.dept_no = d.dept_no
group by dnombre
order by dnombre;
--35
select h.nombre hospital, s.nombre sala, max(salario) maximo
from sala s, plantilla p, hospital h
where h.hospital_cod = p.hospital_cod and
p.sala_cod = s.sala_cod
group by h.nombre, s.nombre;
--36
select e.emp_no empleado, e.apellido nombre,
e.oficio oficio, e.dir jefe, e2.apellido nombre, e2.oficio oficio
from emp e, emp e2
where e.dir = e2.emp_no
order by 2;
--37
SELECT APELLIDO, OFICIO, LOC
FROM EMP E, DEPT2 D
WHERE E.DEPT_NO = D.DEPT_NO
AND UPPER(OFICIO) IN ('ANALISTA');
--38
SELECT E.EMP_NO AS empleado, E.APELLIDO AS nombre,
       E.OFICIO AS oficio, E.DIR AS jefe,
       E2.APELLIDO AS nombre_jefe, E2.OFICIO AS oficio_jefe
FROM EMP E
LEFT JOIN EMP E2 ON E.DIR = E2.EMP_NO
ORDER BY 2;
--39
SELECT e.emp_no AS empleado, e.apellido AS nombre, e.oficio AS oficio,
       e.dir AS jefe, e2.apellido AS nombre_jefe, e2.oficio AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY 2;

--40
SELECT e.emp_no AS empleado, e.apellido AS nombre, e.oficio AS oficio,
       e.dir AS jefe, e2.apellido AS nombre_jefe, e2.oficio AS oficio_jefe
FROM emp e, emp e2
WHERE e.dir = e2.emp_no
ORDER BY 2;


--41
SELECT apellido, dept_no, oficio
FROM emp
WHERE oficio IN (SELECT oficio FROM emp WHERE dept_no = 20)
  AND oficio NOT IN (SELECT oficio FROM emp e, dept2 d WHERE e.dept_no = d.dept_no AND UPPER(dnombre) = 'VENTAS');

--42
SELECT emp_no, dept_no, apellido
FROM emp
WHERE dept_no IN (20, 30)
  AND salario > (SELECT 2 * MIN(salario) FROM emp)
  AND UPPER(oficio) NOT LIKE 'PRES%';

---43
select apellido, turno, funcion, salario
from plantilla
where salario > (select max(salario) + 500000

from plantilla
where upper(turno) = 'M')
and funcion in (select funcion
from plantilla
where upper(apellido) like 'NU%');
---44
SELECT apellido, FECHA_ALTA AS Fecha
FROM emp
WHERE FECHA_ALTA = (SELECT MIN(FECHA_ALTA) FROM emp);



--45
select apellido, oficio
from emp
where oficio in (select oficio
from emp
where upper(apellido) = 'JIMENEZ');
--46
select apellido, oficio, salario, dept_no
from emp
where salario > (select max(salario)
from emp
where dept_no = 30);
---47
select apellido, oficio
from emp
where dept_no = 20
and upper(oficio) in (select oficio

from emp e, dept2 d
where upper(dnombre)= 'VENTAS' and
e.dept_no = d.dept_no);
--48
select apellido, oficio
from emp
where salario <> (select max(salario)
from emp)

and salario <> (select min(salario)
from emp);
--49
SELECT MAX(salario) AS maximo, dept_no
FROM emp
WHERE SUBSTRING(apellido, 1, 1) NOT IN
  (SELECT SUBSTRING(apellido, 1, 1)
   FROM emp
   WHERE salario, dept_no) IN
     (SELECT MAX(salario), dept_no
      FROM emp
      GROUP BY dept_no))
GROUP BY dept_no
HAVING COUNT(*) > 2;


--50
SELECT COUNT(DISTINCT oficio) AS numero, d.dept_no AS num_dep, d.dnombre AS nombre
FROM dept2 d
LEFT JOIN emp e ON d.dept_no = e.dept_no
WHERE d.dept_no NOT IN
  (SELECT dept_no
   FROM emp
   WHERE UPPER(oficio) LIKE 'VENDE%')
GROUP BY d.dept_no, d.dnombre;

--51
select e1.dept_no dep, e1.salario maximo,
e1.apellido apellido, e2.dept_no dep,
e2.salario m�nimo, e2.apellido apellido
from emp e1, emp e2
where e1.salario in (select max(salario)
from emp)
and e2.salario in (select min(salario)
from emp);
--52
SELECT d.dnombre AS DNOMBRE, COUNT(*) AS EMPLEADOS
FROM dept2 AS d
JOIN emp AS e ON d.dept_no = e.dept_no
WHERE MONTH(e.FECHA_ALTA) = 12
GROUP BY d.dnombre
HAVING COUNT(*) = (SELECT MAX(contador) 
                   FROM (SELECT COUNT(*) AS contador
                         FROM emp
                         WHERE MONTH(FECHA_ALTA) = 12
                         GROUP BY dept_no) AS subquery);



--53
SELECT dept_no, MIN(salario) AS m�nimo, MAX(salario) AS m�ximo
FROM emp
WHERE dept_no NOT IN (
    SELECT d.dept_no
    FROM dept2 AS d
    WHERE SUBSTRING(d.dnombre, 1, 1) NOT IN (
        SELECT SUBSTRING(apellido, 1, 1)
        FROM emp
    )
)
GROUP BY dept_no
HAVING (MAX(salario) - MIN(salario)) > (
    SELECT AVG(salario)
    FROM emp
);

--54
SELECT d.dnombre, e.dept_no AS numero, e.apellido, e.fecha_alta
FROM emp AS e
JOIN dept2 AS d ON e.dept_no = d.dept_no
WHERE e.fecha_alta = (
    SELECT MAX(e2.fecha_alta)
    FROM emp AS e2
    WHERE e.dept_no = e2.dept_no
    GROUP BY e2.dept_no
);

--55
WITH AvgSalaries AS (
    SELECT dept_no, AVG(salario) AS avg_salario
    FROM emp
    GROUP BY dept_no
)
SELECT e.apellido, e.oficio, e.dept_no
FROM emp AS e
WHERE e.salario = (
    SELECT MAX(salario)
    FROM emp AS e2
    WHERE e.dept_no = e2.dept_no
)
AND e.dept_no IN (
    SELECT dept_no
    FROM AvgSalaries
    WHERE avg_salario = (
        SELECT MAX(avg_salario)
        FROM AvgSalaries
    )
);

--56
select e1.apellido, e1.oficio, e1.dept_no
from emp e1
where exists (select *

from emp e2
where e1.emp_no = e2.dir)

order by apellido;
--57
select apellido, empleado_no
from plantilla p
where salario > (select avg(salario)
from plantilla p2
where p2.hospital_cod = p.hospital_cod);
--58             -----INSERCIONES
INSERT INTO plantilla
(hospital_cod, sala_cod, empleado_no, apellido, funcion, turno, salario)
VALUES (22, 2, 1234, 'Garcia J.', 'Enfermo', 'M', 3000000);

--59
insert into plantilla
values (22,2,1234,'Garcia J.','Enfermero','M',3000000)
--60
insert into plantilla (hospital_cod, sala_cod, empleado_no, apellido)
values (2, 22, 1234, 'Garcia J');


--61
insert into plantilla
(hospital_cod, sala_cod, empleadono, apellido)
values (2,22,1234,'Garcia J');
En esta inserci�n no se contemplan todos los campos de la
tabla, )Falla la inserci�n?.
No

              -------ACTUALIZACIONES
--62
update enfermo
set direccion = 'Alcala 411'
where inscripcion = 74835

--63
update enfermo
set direccion = null
--64
update enfermo
set direccion = 'Alcala 411'
where inscripcion = 74835
--65
update enfermo
set direccion = (select direccion from enfermo where inscripcion = 14024),
    fecha_nac = (select fecha_nac from enfermo where inscripcion = 14024)
where inscripcion = 10995;

--66

SELECT* INTO hospitales from hospital
alter table hospitales2 alter column num_cama + (num_cama *0.1);


--67
create table hospitales22
(hospital_cod number(2),
nombre varchar2(15),
direccion varchar2(20),
telefono char(8),
num_cama number(3),
constraint hospital_pk primary key (hosp�tal_cod)
--68
--69
--70
--71
--72
--73
--74
--75

