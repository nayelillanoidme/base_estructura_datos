use SQL
CREATE TABLE Proveedores
(P# CHAR(3),
PNombre VARCHAR(20),
Categoria INT,
Ciudad VARCHAR(30),
CONSTRAINT P#_pk PRIMARY KEY(P#))


insert into proveedores
values ('p1','carlos',20,'sevilla'),
('p2','juan',10,'madrid'),
('p3','jose',30,'sevilla'),
('p4','inma',20,'sevilla'),
('p5','eva',30,'caceres')


CREATE TABLE Componentes
(C# CHAR(3),
CNombre VARCHAR(20),
Color CHAR(10),
Peso INT,
Ciudad VARCHAR(30),
CONSTRAINT C#_pk PRIMARY KEY(C#))
/*Tabla Componentes*/
insert into componentes
values ('c1','x3a','rojo',12,'sevilla')

insert into componentes
values ('c2','b85','verde',17,'madrid'),
('c3','c4b','azul',17,'malaga'),
('c4','c4b','rojo',14,'sevilla'),
('c5','vt8','azul',12,'madrid'),
('c6','c30','rojo',19,'sevilla')
/*Tabla componentes*/
SELECT DISTINCT COLOR, CIUDAD
FROM COMPONENTES

SELECT c#
FROM componentes
WHERE peso = (SELECT MIN(peso)
FROM componentes );


CREATE TABLE Articulos
(T# CHAR(3),
TNombre VARCHAR(20),
Ciudad VARCHAR(30),
CONSTRAINT T#_pk PRIMARY KEY(T#))
/*Tabla Articulos*/
insert into articulos
values ('t1','clasificadora','madrid'),
('t2','perforadora','malaga'),
('t3','lectora','caceres'),
('t4','consola','caceres'),
('t5','mezcladora','sevilla'),
('t6','terminal','barcelona'),
('t7','cinta','sevilla')

insert into articulos(t#,tnombre)
values ('t1','clasificadora')

select * from articulos

insert into articulos(tnombre, ciudad, t#)
values ('perforadora', 'malaga', 't2')

SELECT *
FROM Articulos
WHERE ciudad='caceres'

SELECT T#, CIUDAD
FROM ARTICULOS
WHERE CIUDAD LIKE '%D'
OR CIUDAD LIKE '%E%'

SELECT TNOMBRE
FROM ARTICULOS INNER JOIN ENVIOS
ON ENVIOS.P#='P1' AND ENVIOS.T#=ARTICULOS.T#

SELECT DISTINCT ENVIOS.C#
FROM ARTICULOS INNER JOIN ENVIOS
ON ARTICULOS.T#=ENVIOS.T#
WHERE ARTICULOS.CIUDAD='MADRID'


CREATE TABLE Envios
(P# CHAR(3) CONSTRAINT P# REFERENCES Proveedores(P#),
C# CHAR(3) CONSTRAINT C# REFERENCES Componentes(C#),
T# CHAR(3) CONSTRAINT T# REFERENCES Articulos(T#),
Cantidad INT)
/*Tabla Envios*/
insert into envios
values ('p1','c1','t1',200),
('p1','c1','t4',700),
('p2','c3','t1',400),
('p2','c3','t2',200),
('p2','c3','t3',200),
('p2','c3','t4',500),
('p2','c3','t5',600),
('p2','c3','t6',400),
('p2','c3','t7',800),
('p2','c5','t2',100),
('p3','c3','t1',200),
('p3','c4','t2',500),
('p4','c6','t3',300),
('p4','c6','t7',300),
('p5','c2','t2',200),
('p5','c2','t4',100),
('p5','c5','t4',500),
('p5','c5','t7',100),
('p5','c6','t2',200),
('p5','c1','t4',100),
('p5','c3','t4',200),
('p5','c4','t4',800),
('p5','c5','t5',400),
('p5','c6','t4',500)

SELECT P#
FROM ENVIOS
WHERE T#= 'T1'

SELECT P#
FROM ENVIOS
WHERE T#='T1' AND C#='C1'

SELECT ENVIOS.P#
FROM ENVIOS
WHERE T#='T1'
INTERSECT
SELECT ENVIOS.P#
FROM ENVIOS
WHERE T#='T2'

SELECT DISTINCT ENVIOS.P#
FROM (ENVIOS JOIN COMPONENTES
ON ENVIOS.C#=COMPONENTES.C#)
JOIN ARTICULOS
ON ENVIOS.T#=ARTICULOS.T#
WHERE (ARTICULOS.CIUDAD='SEVILLA' OR ARTICULOS.CIUDAD='MADRID') AND
(COMPONENTES.COLOR='ROJO')

SELECT DISTINCT C#
FROM ENVIOS
WHERE T# IN( SELECT T#
FROM ARTICULOS
WHERE CIUDAD='SEVILLA')
AND P# IN ( SELECT P#
FROM PROVEEDORES
WHERE CIUDAD='SEVILLA')

SELECT DISTINCT T#
FROM ENVIOS
WHERE C# IN( SELECT DISTINCT C#
FROM ENVIOS
WHERE P#='P1')

SELECT P.Ciudad ,E.C#, A.Ciudad
FROM Envios E, Proveedores P, Articulos A
WHERE E.P#=P.P# AND E.T#=A.T#;

SELECT P.Ciudad, C#, A.Ciudad
FROM Envios E, Proveedores P , Articulos A
WHERE E.P#=P.P# AND E.T#=A.T# AND P.Ciudad <> A.Ciudad
