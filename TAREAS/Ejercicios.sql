/**1. Crear una base de datos de nombre biblioteca, y crear las tablas tal y como se haría en SQL
(asuma los tipos de datos que estime conveniente), una vez creadas, agregar a la tabla
Usuario el campo sexo de un solo carácter y agregar a la tabla Libro el campo índice de tipo
entero.*/
/* Crea la base de datos */
CREATE DATABASE biblioteca;

/* Habilitamos la base de datos */
USE biblioteca;

/* Crear las tablas LIBRO-USUARIO-CLASE-PRESTAMO */
CREATE TABLE clase (
  Clave char(10) PRIMARY KEY,
  Tiempo_de_prestamo int
);

CREATE TABLE libro (
  codigo varchar(10) PRIMARY KEY,
  autor varchar(50),
  titulo varchar(50),
  editor varchar(30),
  prestado int DEFAULT 1,
  clase char(10) REFERENCES clase(Clave)
);

CREATE TABLE Usuario (
  secuencia varchar(10) PRIMARY KEY,
  nombre varchar(50),
  direccion varchar(50),
  fecha_ingreso DATETIME NOT NULL
);

CREATE TABLE prestamo (
  codigo varchar(10),
  secuencia varchar(10),
  fecha_inicio datetime,
  PRIMARY KEY (codigo, secuencia),
  FOREIGN KEY (codigo) REFERENCES libro(codigo),
  FOREIGN KEY (secuencia) REFERENCES usuario(secuencia)
);

ALTER TABLE Usuario ADD sexo char(1);
ALTER TABLE libro ADD indice int;


/*2. Dar un ejemplo de inserción en cada una de las tablas. */
/* Tabla Clase */
INSERT INTO clase VALUES(1, 15);
INSERT INTO clase VALUES(2, 30);
INSERT INTO clase VALUES(3, 365);
/* Tabla Libro */
INSERT INTO libro VALUES ('INF01', 'Juan','Los problemas', 'MG', 1,1,1);
INSERT INTO libro VALUES ('INF02','Mateo','Psicología','LL', 1,1,1);
INSERT INTO libro VALUES ('FIS01', 'María', 'Fundamentos físicos', 'RO-MO',2,1,1);
INSERT INTO libro VALUES ('MAT02','Lucas', 'Cálculo', 'MM', 3,1,1);
INSERT INTO libro VALUES ('MAT0', 'Susana', 'Matemáticas discretas', 'MM',2,1,1);
/* Tabla Usuario */
INSERT INTO usuario VALUES(1, 'juan', 'Av. Lima 123','1/1/2023','F');
INSERT INTO usuario VALUES(2, 'mateo', 'Jr. Huancayo 2','7/8/2023','F');
INSERT INTO usuario VALUES(3, 'maria', 'Jr. Puno 8','6/2/2023','M');
INSERT INTO usuario VALUES(4, 'lucas', 'Jr. Tacna 91','3/4/2023','F');
INSERT INTO usuario VALUES(5, 'susana', 'Psj. Ayachucho 333','2/5/1923','M');
/* Tabla prestamo */
INSERT INTO prestamo VALUES('INF01', 1, '1/6/2023');
INSERT INTO prestamo VALUES('INF02', 1, '1/6/2023');
INSERT INTO prestamo VALUES('FIS01', 2, '7/5/2023');
INSERT INTO prestamo VALUES('MAT02', 3, '2/8/2023');
INSERT INTO prestamo VALUES('MAT05', 3, '2/6/2023');

/*3. Modificar la clase de los préstamos de tipo 3 de forma que la duración total sea de 180 días en vez de 365*/
UPDATE clase SET tiempo_de_prestamo=180 WHERE Clave=3; 

/*Borrar aquellos usuarios cuyo número de secuencia sea mayor de 3.*/
DELETE FROM usuario WHERE secuencia>3;

/*Seleccionar, alfabéticamente, el título de todos los libros del área de informática (código
INFXX) dónde XX actúan como comodines, es decir, podrán tomar dos valores cualesquiera
(por ejemplo, INF00, INF01, INF99, ...)*/
SELECT titulo FROM libro WHERE codigo LIKE 'IN%' ORDER BY titulo;


/*Seleccionar la fecha de inicio de los préstamos que tienen los usuarios que viven en la
calle “Jr. Puno”*/
SELECT fecha_inicio FROM prestamo, usuario
WHERE prestamo.secuencia=usuario.secuencia
AND usuario.direccion='Jr. Puno 8';

/*Seleccionar la fecha de inicio de los préstamos de los libros de la editorial MM que tienen
los usuarios que ingresaron después del año 2005.*/
SELECT fecha_inicio
FROM prestamo, libro, usuario
WHERE prestamo.secuencia=usuario.secuencia
AND prestamo.codigo=libro.codigo
AND editor='MM' AND fecha_ingreso>'31/12/2005';

/*Devolver, para cada editor, la cantidad de libros prestados que tiene.*/
SELECT editor, count(libro.codigo)
FROM libro, prestamo
WHERE prestamo.codigo=libro.codigo
GROUP BY editor;

/*Seleccionar el nombre de aquellos usuarios cuyo número de secuencia es menor que
diez veces la clase de cualquier libro que tenga.*/
SELECT nombre
FROM usuario
WHERE secuencia < ANY (SELECT clase*10
FROM libro, prestamo
WHERE prestamo.codigo=libro.codigo
AND prestamo.secuencia=usuario.secuencia);

/*Seleccionar la fecha de ingreso de aquellos usuarios tales que todos los libros que tienen
prestados tienen un código menor que su número de secuencia.*/
SELECT fecha_ingreso
FROM usuario
WHERE secuencia > ALL (SELECT codigo
FROM prestamo WHERE prestamo.secuencia=usuario.secuencia);

/*Crear una vista (libro_x_persona_vw) que contenga los datos de los libros (código, título
y autor) y de las personas (secuencia, nombre y sexo) que tienen prestados los libros.
Insertar, si es posible, una fila de valores en la vista.*/
CREATE VIEW libro_persona
AS (SELECT libro.codigo, libro.autor, usuario.secuencia, usuario.nombre, usuario.sexo
FROM libro, usuario, prestamo
WHERE libro.codigo=prestamo.codigo AND Usuario.secuencia=prestamo.secuencia)
